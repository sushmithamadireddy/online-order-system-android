package com.example.s528798.simplysiamfinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EmptyCart extends AppCompatActivity {

    Button b1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty_cart);

        b1 = (Button) findViewById(R.id.getStarted);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EmptyCart.this,MenuPage.class));
            }
        });

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(EmptyCart.this,MenuPage.class));
    }
}
