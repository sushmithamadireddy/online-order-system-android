package com.example.s528798.simplysiamfinal;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.s528798.simplysiamfinal.Database.Database;
import com.example.s528798.simplysiamfinal.Model.Order;
import com.example.s528798.simplysiamfinal.Model.Rating;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;
import com.tapadoo.alerter.Alerter;

import org.w3c.dom.Text;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.goncalves.pugnotification.notification.PugNotification;


public class ThankYou extends AppCompatActivity implements RatingDialogListener {

    TextView grandTotal;
    Button home;
    RatingBar ratingBar;
    FloatingActionButton rating;
    List<Order> cart = new ArrayList<>();
    TextView summary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        summary = (TextView) findViewById(R.id.food_description);
        rating = (FloatingActionButton) findViewById(R.id.buton_rating);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        grandTotal = (TextView) findViewById(R.id.food_price);
        home = (Button) findViewById(R.id.home);
        Intent i = getIntent();
        double grandPrice = i.getDoubleExtra("Grand Total",0.0);

        String dineOrDelivery  = i.getStringExtra("DineOrDelivery");
        String customers  = i.getStringExtra("Customers");
        String type  = i.getStringExtra("TYPE");
        String time  = i.getStringExtra("Time");

        OrderType ot = new OrderType();
        String address  = i.getStringExtra("TypeofOrder");

        if(type.equals("DELIVERY")){
            summary.setText("Your order has been placed!\n"+"Delivery Time : In 30 minutes\n"+" Address : "+ ot.getAddress());
        }
        else if(type.equals("DINE")){

                if( time .equals("23:15")){
                    //time = "Arrive in 1 hour, else order will be cancelled";
                    summary.setText("A table has been reserved!\n "+" Number of people: "+ customers);
                    if( customers == null){
                        summary.setText("A table has been reserved!\n");
                    }
                }
                else{
                    summary.setText("A table has been reserved!\n Time: "+time+"\n Number of people: "+ customers);
                }



        }
        else if(type.equals("CARRY")){
            summary.setText("Your order has been placed!\n"+" Order will be ready in 15 minutes");
            new CountDownTimer(9000, 1000) {

                public void onTick(long millisUntilFinished) {
                    summary.setText("Your order has been placed!\n Preparing your food: " + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    summary.setText("Done!");
                    Alerter.create(ThankYou.this)
                            .setTitle("Simply Siam")
                            .setDuration(10000)
                            .enableVibration(true)
                            .enableSwipeToDismiss()
                            .setText("Your order is ready")
                            .setBackgroundColorRes(R.color.accent) // or setBackgroundColorInt(Color.CYAN)
                            .show();

                }
            }.start();
        }
        Locale locale = new Locale("en", "US");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
        grandTotal.setText(" " + fmt.format(grandPrice));


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Database(getBaseContext()).clearCart();
                startActivity(new Intent(ThankYou.this,MainActivity.class));
                finish();
            }
        });

        rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRatingDialog();
            }
        });
    }

    @Override
    public void onBackPressed() {
        new Database(getBaseContext()).clearCart();
        startActivity(new Intent(ThankYou.this,MainActivity.class));
        finish();
    }


    private void showRatingDialog() {

        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                //.setNoteDescriptions(Arrays.asList("Very Bad","Not Good","Quite Ok","Very Good","Excellent"))
                .setDefaultRating(4)
                .setTitle("Rate this food")
                .setDescription("Please give some stars and give your feedback")
                .setTitleTextColor(R.color.colorPrimaryDark)
                .setDescriptionTextColor(R.color.colorPrimaryDark)
                .setHint("Please write your comment here...")
                .setHintTextColor(R.color.colorAccent)
                .setCommentTextColor(android.R.color.white)
                .setCommentBackgroundColor(R.color.colorPrimaryDark)
                .setWindowAnimation(R.style.RatingDialogFadeAnim)
                .create(ThankYou.this).show();



    }

    @Override
    public void onPositiveButtonClicked(int i, String s) {
        Toast.makeText(this, "Thankyou for your feedback", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNegativeButtonClicked() {
        Toast.makeText(this, "Feedback canceled", Toast.LENGTH_SHORT).show();
    }
}
