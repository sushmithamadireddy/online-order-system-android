package com.example.s528798.simplysiamfinal;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.s528798.simplysiamfinal.User.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class Main_SignUp_Page extends AppCompatActivity {


    EditText emailId,password,mobile;
    Button btn_signUp;
    String email,pass,num;
    private FirebaseAuth mAuth;
    MaterialDialog md;
    FirebaseUser currentUser;
    FirebaseAuth.AuthStateListener mAuthListener;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__sign_up__page);

        emailId = (EditText) findViewById(R.id.loginemailid);
        password = (EditText) findViewById(R.id.loginpassword);
        mobile = (EditText) findViewById(R.id.loginNumber);
        btn_signUp = (Button) findViewById(R.id.signupbtn);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        myRef = database.getReference();



        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    // NOTE: this Activity should get onpen only when the user is not signed in, otherwise
                    // the user will receive another verification email.
                            sendVerificationEmail();
                } else {
                    // User is signed out

                }
                // ...
            }
        };

        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                email = emailId.getText().toString();
                pass = password.getText().toString();
                num = mobile.getText().toString();

                if(TextUtils.isEmpty(email)){
                    emailId.setError("Email Required");
                }

                if(!isValidEmailAddress(email)){
                    emailId.setError("Enter valid email");
                }

                if(TextUtils.isEmpty(pass)){
                    password.setError("Password Required");
                }

                if(!isValidPassword(pass)){
                    password.setError("Password must contain:\n" +
                            "- Uppercase,Lowercase\n" +
                            "- 6 Charecters\n" +
                            "- 1 Special Charecter");
                }
                if(TextUtils.isEmpty(num)){
                    mobile.setError("Number Required");
                }
                if(!isValidNumber(num)){
                    mobile.setError("Invalid mobile number");
                }

                else{

                    email = emailId.getText().toString();
                    pass = password.getText().toString();
                    num = mobile.getText().toString();

                    User user1 = new User(email,pass,num);
                    myRef = database.getReference("User Email");
                    myRef.setValue(user1.getEmail());
                    signUpUserwithEmailandPassword(email,pass,num);
                }



            }
        });

    }

    private void sendVerificationEmail() {


            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

            user.sendEmailVerification()
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                // email sent


                                // after email is sent just logout the user and finish this activity
                                FirebaseAuth.getInstance().signOut();
                                Toast.makeText(Main_SignUp_Page.this, "Instructions sent to email", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(Main_SignUp_Page.this, Main_Login_Page.class));
                                finish();
                            }
                            else
                            {
                                Toast.makeText(Main_SignUp_Page.this, "Unknown error", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }


    private void signUpUserwithEmailandPassword(final String email, final String pass, String mobile) {

        //Toast.makeText(this, ""+email, Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, ""+pass, Toast.LENGTH_SHORT).show();

        md = new MaterialDialog.Builder(Main_SignUp_Page.this)
                .title("Creating Account")
                .cancelable(false)
                .contentColor(Color.parseColor("#71DA98"))
                .titleColor(Color.parseColor("#56C5ED"))
                .content("Please wait")
                .progress(true, 0)
                .show();


        mAuth.createUserWithEmailAndPassword(email, pass)
                                    .addOnCompleteListener(Main_SignUp_Page.this, new OnCompleteListener<AuthResult>() {

                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()) {
                                                // Sign in success, update UI with the signed-in user's information
                                                md.dismiss();
                                                Toast.makeText(Main_SignUp_Page.this, "Instructions sent to email", Toast.LENGTH_SHORT).show();
                                                //startActivity(new Intent(Main_SignUp_Page.this,Main_Login_Page.class));
                                                sendVerificationEmail();
                                                finish();

                                            } else {
                                                // If sign in fails, display a message to the user.
                                                //Toast.makeText(Main_SignUp_Page.this, "", Toast.LENGTH_SHORT).show();
                                                md.dismiss();
                                                emailId.setError("Email already exists");

                        }
                    }
                });

    }





    public static boolean isValidEmailAddress(String email) {

        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }


    public static boolean isValidPassword(String password) {

        String ePattern = "^.*(?=.{6,})(?=..*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[.@#$%^&+=]).*$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(password);
        return m.matches();
    }

    public static boolean isValidNumber(String number) {

        String ePattern = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(number);
        return m.matches();
    }


    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            startActivity(new Intent(Main_SignUp_Page.this,MainActivity.class));
        }
    }


}
