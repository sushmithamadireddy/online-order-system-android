package com.example.s528798.simplysiamfinal;

public class DeliveryAddress {

    public static String address;

    public DeliveryAddress() {
    }

    public static String getAddress() {
        return address;
    }

    public static void setAddress(String address) {
        DeliveryAddress.address = address;
    }
}
