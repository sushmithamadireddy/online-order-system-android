package com.example.s528798.simplysiamfinal.User;

/**
 * Created by ravit on 3/5/2018.
 */

public class User {

    private String email,password,number;

    public User() {
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User(String email, String password, String number) {
        this.email = email;
        this.password = password;
        this.number = number;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
