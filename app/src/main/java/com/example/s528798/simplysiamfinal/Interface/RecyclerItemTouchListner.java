package com.example.s528798.simplysiamfinal.Interface;

import android.support.v7.widget.RecyclerView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;

/**
 * Created by ravit on 3/7/2018.
 */

public interface RecyclerItemTouchListner {

    void onDialogTimeSet(int reference, int hourOfDay, int minute);

    void onSwipe(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, int direction, int position);

    ;

    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int adapterPosition);

    void onPlaceSelected(Place place);

    void onError(Status status);
}
