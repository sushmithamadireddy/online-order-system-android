package com.example.s528798.simplysiamfinal.Interface;

import android.view.View;

/**
 * Created by ravit on 2/28/2018.
 */

public interface ItemClickListener {
    void onClick(View view,int position,boolean isLongClick);
}
