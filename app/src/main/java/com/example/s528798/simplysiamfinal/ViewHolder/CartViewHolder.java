package com.example.s528798.simplysiamfinal.ViewHolder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.s528798.simplysiamfinal.Common.Common;
import com.example.s528798.simplysiamfinal.Interface.ItemClickListener;
import com.example.s528798.simplysiamfinal.R;

/**
 * Created by ravit on 3/7/2018.
 */

public class CartViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        ,View.OnCreateContextMenuListener{

    public TextView txt_cart_name,txt_price;
    public ElegantNumberButton btn_quantity;
    ImageView cart_image;
    public RelativeLayout view_background;
    public LinearLayout view_foreground;

    CardView cardList;

    private ItemClickListener itemClickListener;

    public void setTxt_cart_name(TextView txt_cart_name){
        this.txt_cart_name = txt_cart_name;
    }
    public CartViewHolder(View itemView){
        super(itemView);
        txt_cart_name  = (TextView) itemView.findViewById(R.id.cart_item_name);
        txt_price  = (TextView) itemView.findViewById(R.id.cart_item_price);
        btn_quantity  = (ElegantNumberButton) itemView.findViewById(R.id.btn_quantity);
        //img_cart_count = (ImageView) itemView.findViewById(R.id.img_cart_count);
        cardList = (CardView) itemView.findViewById(R.id.cardList);
        itemView.setOnCreateContextMenuListener(this);
        cart_image = itemView.findViewById(R.id.cart_image);
        view_background = (RelativeLayout) itemView.findViewById(R.id.viewBackGround);
        view_foreground = (LinearLayout) itemView.findViewById(R.id.view_foreground);


        cardList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.setHeaderTitle("Select action");
        menu.add(0,0,getAdapterPosition(), Common.DELETE);
    }
}
