package com.example.s528798.simplysiamfinal.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.example.s528798.simplysiamfinal.Model.Order;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravit on 3/1/2018.
 */

public class Database extends SQLiteAssetHelper {

    private static final String DB_NAME="EatDB.db";
    private static final int DB_VER=1;



    public Database(Context context){
        super(context, DB_NAME, null, DB_VER);
    }

    public List<Order> getCarts(){

        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String[] sqlSelect = {"ID","ProducId","ProductName","Quantity","Price","Discount","Image"};
        String sqlTable = "OrderDetail";

        qb.setTables(sqlTable);
        Cursor c = qb.query(db,sqlSelect,null,null ,null,null,null);

        final List<Order> result = new ArrayList<>();

        if(c.moveToFirst())
        {
            do{
                result.add(new Order(
                        c.getString(c.getColumnIndex("ProducId")),
                        c.getString(c.getColumnIndex("ProductName")),
                        c.getString(c.getColumnIndex("Quantity")),
                        c.getString(c.getColumnIndex("Price")),
                        c.getString(c.getColumnIndex("Discount")),
                        c.getString(c.getColumnIndex("Image"))
                ));
            }while (c.moveToNext());

        }
            return result;
    }

    public void addToCart(Order order){
        SQLiteDatabase db = getReadableDatabase();



        String query = String.format("INSERT OR REPLACE INTO OrderDetail(ProducId,ProductName,Quantity,Price,Discount,Image) VALUES('%s','%s','%s','%s','%s','%s')",

                order.getProductId(),
                order.getProductName(),
                order.getQuantity(),
                order.getPrice(),
                order.getDiscount(),
                order.getImage());

        db.execSQL(query);
    }

    public void clearCart(){
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM OrderDetail");
        db.execSQL(query);
    }




    public void updateCart(Order order) {

        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("UPDATE OrderDetail SET Quantity= '%s' WHERE ID = '%s'",order.getQuantity(),order.getProductId());
        db.execSQL(query);
    }

    public void removeFromCart(String productId) {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM OrderDetail WHERE ProducId='%s'",productId);

        db.execSQL(query);

    }
}
