package com.example.s528798.simplysiamfinal;

import android.content.Intent;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.s528798.simplysiamfinal.Database.Database;
import com.example.s528798.simplysiamfinal.Interface.ItemClickListener;
import com.example.s528798.simplysiamfinal.Model.Category;
import com.example.s528798.simplysiamfinal.Model.Food;
import com.example.s528798.simplysiamfinal.Model.Order;
import com.example.s528798.simplysiamfinal.ViewHolder.FoodViewHolder;
import com.example.s528798.simplysiamfinal.ViewHolder.MenuViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class FoodList extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    ImageView addToCart;
    FirebaseDatabase database;
    DatabaseReference foodList;
    String categoryId = "";
    String foodId = "";
    Food curentFood;
    private FirebaseRecyclerAdapter<Food,FoodViewHolder> adapter;
    Query query;
    FloatingActionButton fab ;
    FloatingActionButton fbtn;

    private FirebaseRecyclerAdapter<Food,FoodViewHolder> searchAdapter;
    List<String> suggestList = new ArrayList<>();
    MaterialSearchBar materialSearchBar;
    RelativeLayout mLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);
        database = FirebaseDatabase.getInstance();
        foodList = database.getReference("Food");
        //Query query = category.limitToFirst(50);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_food);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        fab = (FloatingActionButton) findViewById(R.id.floatingActionButton1);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FoodList.this,Cart.class));
            }
        });



        if(getIntent() != null)
            categoryId = getIntent().getStringExtra("CategoryId");
        if(!categoryId.isEmpty() && categoryId !=null){
            query  = foodList.orderByChild("MenuId").equalTo(categoryId);
            loadListFood(categoryId);
        }


        materialSearchBar = (MaterialSearchBar) findViewById(R.id.searchBar);
        materialSearchBar.setHint("Enter your Food");
        loadSuggest();
        materialSearchBar.setLastSuggestions(suggestList);
        materialSearchBar.setCardViewElevation(10);
        materialSearchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                List<String> suggest = new ArrayList<>();
                for(String search:suggestList){
                    if(search.toLowerCase().contains(materialSearchBar.getText().toLowerCase()))
                            suggest.add(search);


                }

                    materialSearchBar.setLastSuggestions(suggest);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        materialSearchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {
                if(!enabled)
                        recyclerView.setAdapter(adapter);
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                startSearch(text);

            }

            @Override
            public void onButtonClicked(int buttonCode) {

            }
        });

    }

    private void startSearch(CharSequence text) {

        query  = foodList.orderByChild("Name").equalTo(text.toString());
        FirebaseRecyclerOptions<Food> options = new FirebaseRecyclerOptions.Builder<Food>()
                .setQuery(query, Food.class)
                .build();

       searchAdapter =
                new FirebaseRecyclerAdapter<Food,FoodViewHolder>(options) {

                    @Override
                    public FoodViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                        View view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.food_item,parent,false);
                        return new FoodViewHolder(view);
                    }

                    @Override
                    protected  void onBindViewHolder(FoodViewHolder holder,int position,Food model){
                        holder.food_name.setText(model.getName());
                        Picasso.with(getBaseContext()).load(model.getImage())
                                .into(holder.food_image);
                        final Food local = model;
                        holder.setItemClickListener(new ItemClickListener() {
                            @Override
                            public void onClick(View view, int position, boolean isLongClick) {
                                Intent foodDetail = new Intent(FoodList.this,FoodDetail.class);
                                foodDetail.putExtra("FoodId",adapter.getRef(position).getKey());
                                startActivity(foodDetail);
                            }
                        });
                    }
                };
        searchAdapter.startListening();
        recyclerView.setAdapter(searchAdapter);

    }

    private void loadSuggest() {

        foodList.orderByChild("MenuId").equalTo(categoryId)
        .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    Food item = dataSnapshot1.getValue(Food.class);
                    suggestList.add(item.getName());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void loadListFood(String categoryId) {


        FirebaseRecyclerOptions<Food> options = new FirebaseRecyclerOptions.Builder<Food>()
                .setQuery(query, Food.class)
                .build();

        adapter =
                new FirebaseRecyclerAdapter<Food,FoodViewHolder>(options) {

                    @Override
                    public FoodViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                        View view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.food_item,parent,false);
                        return new FoodViewHolder(view);
                    }

                    @Override
                    protected  void onBindViewHolder(FoodViewHolder holder,int position,Food model){
                        holder.food_name.setText(model.getName());
                        holder.food_price.setText(String.format("$ %s",model.getPrice()).toString());
                        Picasso.with(getBaseContext()).load(model.getImage())
                                .into(holder.food_image);
                        final Food local = model;
                        holder.setItemClickListener(new ItemClickListener() {
                            @Override
                            public void onClick(View view, int position, boolean isLongClick) {
                                Intent foodDetail = new Intent(FoodList.this,FoodDetail.class);
                                foodDetail.putExtra("FoodId",adapter.getRef(position).getKey());
                                startActivity(foodDetail);
                            }
                        });
                    }
                };
        adapter.startListening();
        recyclerView.setAdapter(adapter);
    }

}
