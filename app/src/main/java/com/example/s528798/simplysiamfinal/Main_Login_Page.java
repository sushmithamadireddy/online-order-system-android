package com.example.s528798.simplysiamfinal;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.s528798.simplysiamfinal.Database.Database;
import com.example.s528798.simplysiamfinal.User.User;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.irozon.sneaker.Sneaker;
import java.io.IOException;

import java.util.Arrays;

public class Main_Login_Page extends AppCompatActivity {

    Button login_btn;
    ImageButton googleButton,facebookButton,anonymous;
    TextView mLogo,register,forgotPass;
    String email,pass;
    EditText emailId,password;
    private FirebaseAuth mAuth;
    MaterialDialog md;
    FirebaseUser currentUser;
    GoogleSignInOptions gso;
    GoogleApiClient mGoogleApiClient;
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 123;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef;
    private String recoveryEmail;

    CallbackManager callbackManager;
    private static final String EMAIL = "email";
    LoginManager loginManager;
    LoginButton loginButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__login__page);
        login_btn = (Button) findViewById(R.id.loginButton);
        googleButton = (ImageButton) findViewById(R.id.googlebtn);

        mLogo = (TextView) findViewById(R.id.logo);
        emailId = (EditText) findViewById(R.id.loginemailid);
        password = (EditText) findViewById(R.id.loginpassword);
        register = (TextView) findViewById(R.id.register);
        forgotPass = (TextView) findViewById(R.id.recoverTV);
        anonymous = (ImageButton) findViewById(R.id.anonymousSignIn);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList(EMAIL));

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                loginButton.setReadPermissions("email", "public_profile");
                loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        handleFacebookAccessToken(loginResult.getAccessToken());
                        md = new MaterialDialog.Builder(Main_Login_Page.this)
                                .title("Logging in")
                                .cancelable(false)
                                .contentColor(Color.parseColor("#71DA98"))
                                .titleColor(Color.parseColor("#56C5ED"))
                                .content("Please wait")
                                .progress(true, 0)
                                .show();
                        startActivity(new Intent(Main_Login_Page.this,MainActivity.class));
                        finish();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(Main_Login_Page.this, "Cancelled", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(Main_Login_Page.this, "Unkown Error", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });



        anonymous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                md = new MaterialDialog.Builder(Main_Login_Page.this)
                        .title("Logging in")
                        .cancelable(false)
                        .contentColor(Color.parseColor("#71DA98"))
                        .titleColor(Color.parseColor("#56C5ED"))
                        .content("Please wait")
                        .progress(true, 0)
                        .show();
                mAuth.signInAnonymously()
                        .addOnCompleteListener(Main_Login_Page.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {

                                    md.dismiss();
                                    startActivity(new Intent(Main_Login_Page.this,MainActivity.class));
                                    finish();

                                } else {
                                    md.dismiss();
                                    Sneaker.with(Main_Login_Page.this)
                                            .setTitle("Error!!")
                                            .setMessage("Unknown error")
                                            .sneakError();

                                }


                            }
                        });
            }
        });



        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Main_Login_Page.this);
                alertDialog.setTitle("Forgot Password!");
                alertDialog.setMessage("Please provide your email: ");
                final EditText editText = new EditText(Main_Login_Page.this);
                //editText.setHint("Optional");
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                );
                editText.setLayoutParams(lp);
                alertDialog.setView(editText);
                alertDialog.setIcon(R.drawable.emailicon);

                recoveryEmail = "";
                alertDialog.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (editText.getText().toString().isEmpty()) {
                            editText.setError("Email Required");
                        } else {
                            new Database(getBaseContext()).clearCart();
                            recoveryEmail = editText.getText().toString().trim();
                            mAuth.sendPasswordResetEmail(recoveryEmail)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Sneaker.with(Main_Login_Page.this)
                                                        .setTitle("Email sent!!")
                                                        .setMessage("Please follow instructions")
                                                        .sneakSuccess();
                                            }
                                            else{
                                                Sneaker.with(Main_Login_Page.this)
                                                        .setTitle("Warning!!")
                                                        .setMessage("Invalid Email/Email does not exist")
                                                        .sneakWarning();
                                            }
                                        }
                                    });

                        }
                    }

                }).setNegativeButton("Cancel", null);
                alertDialog.create().show();
            }
        });




        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        if(currentUser != null){
            checkIfEmailVerified();
            //startActivity(new Intent(Main_Login_Page.this,MainActivity.class));
        }


        Shader shader = new LinearGradient(
                0, 0, 0, mLogo.getTextSize(),
                Color.parseColor("#71DA98"), Color.parseColor("#56C5ED"),
                Shader.TileMode.CLAMP);
        mLogo.getPaint().setShader(shader);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Main_Login_Page.this,Main_SignUp_Page.class));
            }
        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = emailId.getText().toString();
                pass = password.getText().toString();

                if(TextUtils.isEmpty(email)){
                    emailId.setError("Email Required");
                }

                if(TextUtils.isEmpty(pass)){
                    password.setError("Password Required");
                }

                else{
                    md = new MaterialDialog.Builder(Main_Login_Page.this)
                            .title("Creating Account")
                            .cancelable(false)
                            .contentColor(Color.parseColor("#71DA98"))
                            .titleColor(Color.parseColor("#56C5ED"))
                            .content("Please wait")
                            .progress(true, 0)
                            .show();


                    mAuth.signInWithEmailAndPassword(email, pass)
                            .addOnCompleteListener(Main_Login_Page.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        md.dismiss();
                                        checkIfEmailVerified();
                                        //startActivity(new Intent(Main_Login_Page.this,MainActivity.class));

                                    } else {
                                        // If sign in fails, display a message to the user.
                                        //Log.w(TAG, "signInWithEmail:failure", task.getException());
                                        md.dismiss();
                                        //checkIfEmailVerified();
                                        Toast.makeText(Main_Login_Page.this, "Authentication failed,Invalid details",
                                                Toast.LENGTH_SHORT).show();

                                    }

                                }
                            });
                }




            }
        });


        googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);

            }
        });











    }

    private void handleFacebookAccessToken(AccessToken token) {


        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            FirebaseUser user = mAuth.getCurrentUser();

                            //finish();
                        } else {
                            // If sign in fails, display a message to the user.

                            //Toast.makeText(Main_Login_Page.this, "Authentication failed.",
                                    //Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });
    }





    private void checkIfEmailVerified() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        assert user != null;
        if (user.isEmailVerified())
        {
            // user is verified, so you can finish this activity or send user to activity which you want.

            finish();
            startActivity(new Intent(Main_Login_Page.this,MainActivity.class));
            //Toast.makeText(Main_Login_Page.this, "Successfully logged in", Toast.LENGTH_SHORT).show();
        }
        else
        {
            // email is not verified, so just prompt the message to the user and restart this activity.
            // NOTE: don't forget to log out the user.
            FirebaseAuth.getInstance().signOut();
            Toast.makeText(this, "Please verify email address", Toast.LENGTH_SHORT).show();

            //restart this activity

        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        //checkIfEmailVerified();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {

                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);

            } catch (ApiException e) {

                Toast.makeText(this, "Sign in error", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {

        md = new MaterialDialog.Builder(Main_Login_Page.this)
                .title("Signing in")
                .cancelable(false)
                .contentColor(Color.parseColor("#71DA98"))
                .titleColor(Color.parseColor("#56C5ED"))
                .content("Please wait")
                .progress(true, 0)
                .show();

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            md.dismiss();
                            startActivity(new Intent(Main_Login_Page.this,MainActivity.class));
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.

                            Toast.makeText(Main_Login_Page.this, "Authentication Failed", Toast.LENGTH_SHORT).show();

                        }

                    }
                });
    }


}
