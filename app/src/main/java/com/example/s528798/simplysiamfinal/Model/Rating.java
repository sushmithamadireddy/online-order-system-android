package com.example.s528798.simplysiamfinal.Model;

/**
 * Created by ravit on 3/1/2018.
 */

public class Rating {

    private String fooId;
    private String rateValue,comment;

    public Rating() {
    }

    public Rating(String fooId, String rateValue, String comment) {
        this.fooId = fooId;
        this.rateValue = rateValue;
        this.comment = comment;
    }

    public String getFooId() {
        return fooId;
    }

    public void setFooId(String fooId) {
        this.fooId = fooId;
    }

    public String getRateValue() {
        return rateValue;
    }

    public void setRateValue(String rateValue) {
        this.rateValue = rateValue;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
