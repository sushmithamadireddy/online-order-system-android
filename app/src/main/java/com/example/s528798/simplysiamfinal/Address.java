package com.example.s528798.simplysiamfinal;

import android.content.Intent;
import android.location.Geocoder;
import android.net.wifi.WifiConfiguration;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBufferResponse;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

public class Address extends AppCompatActivity implements PlaceSelectionListener {

    EditText mName,mNumber,mAddress;
    Button mSave;
    FirebaseDatabase database ;
    DatabaseReference myref;
    String Fnumber,FAdress;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    AutoCompleteTextView mState,street;
    Intent intent ;
    String st,ct,state,zp;
    int aptNum;
    EditText aptET,cityET,zipET,phoneET;
    StringBuilder sb;
    TextView clr;
    RelativeLayout rl;
    Snackbar snackbar;
    //public AutocompleteFilter typeFilter = new AutocompleteFilter().Builder();


    private static final int REQUEST_SELECT_PLACE = 1;
        OrderType ot;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);


        mState  = (AutoCompleteTextView) findViewById(R.id.state);
        street = (AutoCompleteTextView) findViewById(R.id.street);
        aptET = (EditText) findViewById(R.id.aptNum);
        cityET = (EditText) findViewById(R.id.city);
        zipET = (EditText) findViewById(R.id.zip);
        database = FirebaseDatabase.getInstance();
        myref = database.getReference();
        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        ct = cityET.getText().toString();
        state = mState.getText().toString();
        zp = zipET.getText().toString();
        mSave = (Button) findViewById(R.id.saveAddress);
        sb = new StringBuilder();
        phoneET = (EditText) findViewById(R.id.number);
        clr = (TextView) findViewById(R.id.clear);
        rl = (RelativeLayout) findViewById(R.id.addressLayout);


        ot = new OrderType();


        street.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder
                    (PlaceAutocomplete.MODE_OVERLAY).setBoundsBias(new LatLngBounds(
                    new LatLng(40.345760, -94.892761),
                    new LatLng(40.351190, -94.855923)))
                    .build(Address.this);

            startActivityForResult(intent, REQUEST_SELECT_PLACE);
        } catch (GooglePlayServicesRepairableException |
                GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }
});








        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(street.getText().length() == 0){
                    //street.setError("Field cannot be left blank");
                    Toasty.error(getApplicationContext(), "Street is required", Toast.LENGTH_SHORT, true).show();
                }
                else if(TextUtils.isEmpty(aptET.getText())){
                    //aptET.setError("Field cannot be left blank");
                    Toasty.error(getApplicationContext(), " Apt no is required", Toast.LENGTH_SHORT, true).show();
                }

                else if(TextUtils.isEmpty(phoneET.getText())){
                    //phoneET.setError("Field cannot be left blank");
                    Toasty.error(getApplicationContext(), "phone is required", Toast.LENGTH_SHORT, true).show();
                }

                else if(phoneET.getText().length()>=1 && phoneET.getText().length()<=9){
                   //phoneET.setError("Invalid number");
                    Toasty.warning(getApplicationContext(), "Invalid number", Toast.LENGTH_SHORT, true).show();
                }



                else {

                    sb.append(st).append(" ").append(ct).append(" ").append(state).append(" ").append(zp);
                    Fnumber = phoneET.getText().toString();
                    FAdress = sb.toString();

                        UserInformation info = new UserInformation(Fnumber, FAdress);
                        myref.child(user.getUid()).child("Address").setValue(FAdress);
                        myref.child(user.getUid()).child("Mobile").setValue(Fnumber);
                        Toasty.success(getApplicationContext(), "Address Saved", Toast.LENGTH_SHORT, true).show();
                        ot.setAddress(FAdress);
                        startActivity(new Intent(Address.this, MenuPage.class));

            }

            }
        });






    }

    @Override
    public void onPlaceSelected(Place place) {

        street.setText(place.getName());
        st = street.getText().toString();


    }

    @Override
    public void onError(Status status) {

        Toast.makeText(this, "Place selection failed: " + status.getStatusMessage(),
                Toast.LENGTH_SHORT).show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SELECT_PLACE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                this.onPlaceSelected(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                this.onError(status);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void clearAll(View view) {

        street.setText("");
        aptET.setText("");
        phoneET.setText("");
    }
}
