package com.example.s528798.simplysiamfinal;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ShareActionProvider;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.example.s528798.simplysiamfinal.Database.Database;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.irozon.sneaker.Sneaker;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,BaseSliderView.OnSliderClickListener,ViewPagerEx.OnPageChangeListener {

    private FirebaseAuth firebaseAuth;
    private GoogleApiClient mGoogleSignInClient;
    private FirebaseAuth.AuthStateListener mAuthListner;
    SliderLayout sliderLayout;
    FirebaseDatabase database ;
    LinearLayout imgProfile;
    MaterialDialog md;

    HashMap<String,Integer> Hash_file_maps ;
    Button dine,delivery,carryout;
    private ShareActionProvider mShareActionProvider;
    TextView profileName;
    String SpinnerType = "DINE";



    Intent i;
    OrderType ot;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        firebaseAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = firebaseAuth.getCurrentUser();
        dine = (Button) findViewById(R.id.dineIn);
        delivery = (Button) findViewById(R.id.delivery);
        carryout = (Button) findViewById(R.id.carrryOut);
        final DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();


        i = new Intent(MainActivity.this,MenuPage.class);






        mAuthListner = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser() == null){

                }
            }
        };

        dine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


//                rootRef.child(user.getUid()).child("Type of Delivery").setValue("dine");
                SpinnerType = "DINE";
                ot = new OrderType(SpinnerType);
                ot.setName("DINE");
               // Toast.makeText(MainActivity.this, ""+ot.getName(), Toast.LENGTH_SHORT).show();
                startActivity(i);
            }
        });

        delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
  //              rootRef.child(user.getUid()).child("Type of Delivery").setValue("delivery");
                SpinnerType = "DELIVERY";
                Intent i = new Intent(MainActivity.this,Address.class);
                //i.putExtra("D",SpinnerType);
                ot = new OrderType(SpinnerType);
                ot.setName("DELIVERY");
                startActivity(i);
            }
        });


        carryout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpinnerType = "CARRYOUT";
    //            rootRef.child(user.getUid()).child("Type of Delivery").setValue("carry out");
                //Intent i = new Intent(MainActivity.this,MenuPage.class);
                ot = new OrderType(SpinnerType);
                ot.setName("CARRYOUT");
                startActivity(i);
            }
        });




        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Hash_file_maps = new HashMap<String, Integer>();

        sliderLayout = (SliderLayout)findViewById(R.id.slider);

        Hash_file_maps.put("Simply Siam", R.drawable.thaiwelcome);
        Hash_file_maps.put("Noodle with tofu", R.drawable.tofu);
        Hash_file_maps.put("Papaya carrot salad", R.drawable.carrot);
        Hash_file_maps.put("Fried wonton",R.drawable.wonton);
        Hash_file_maps.put("Chicken laabmenu",R.drawable.laabmenu);

        for(String name : Hash_file_maps.keySet()){

            TextSliderView textSliderView = new TextSliderView(MainActivity.this);
            textSliderView
                    .description(name)
                    .image(Hash_file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(3000);
        sliderLayout.addOnPageChangeListener(this);
    }
    @Override
    protected void onStop() {

        sliderLayout.stopAutoCycle();

        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

        Toast.makeText(this,slider.getBundle().get("extra") + "",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position) {


    }

    @Override
    public void onPageScrollStateChanged(int state) {}










    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Logout from application?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    firebaseAuth.signOut();
                    new Database(getBaseContext()).clearCart();
                    Toast.makeText(MainActivity.this, "Loggedout successfully", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this,Main_Login_Page.class));
                    finish();
                }
            });
                    builder.setNegativeButton("No",null);

                    AlertDialog alrt = builder.create();
                    alrt.show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthListner);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement



        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {

        }
        // menu
        else if (id == R.id.nav_gallery) {

            startActivity(new Intent(MainActivity.this,MenuPage.class));

        }

        else if(id == R.id.nav_manage){
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel: 6605822077"));
            startActivity(intent);
        }


        else if (id == R.id.nav_slideshow) {
            startActivity(new Intent(MainActivity.this,Location.class));

        }  else if (id == R.id.nav_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Simply Siam, simply thai restaurant,/ 314 N Main St, Maryville, MO 64468");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        }

        else if(id == R.id.nav_logout){

            md = new MaterialDialog.Builder(MainActivity.this)
                    .title("Logging out")
                    .cancelable(false)
                    .contentColor(Color.parseColor("#71DA98"))
                    .titleColor(Color.parseColor("#56C5ED"))
                    .content("Please wait")
                    .progress(true, 0)
                    .show();
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        public void onComplete(@NonNull Task<Void> task) {
                            // user is now signed out
                            new Database(getBaseContext()).clearCart();
                            startActivity(new Intent(MainActivity.this, Main_Login_Page.class));
                            finish();
                        }
                    });
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
