package com.example.s528798.simplysiamfinal;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.Notification;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.renderscript.ScriptGroup;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.util.Util;
import com.codetroopers.betterpickers.Utils;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.codetroopers.betterpickers.timepicker.TimePickerBuilder;
import com.codetroopers.betterpickers.timepicker.TimePickerDialogFragment;
import com.example.s528798.simplysiamfinal.Common.Common;
import com.example.s528798.simplysiamfinal.Database.Database;
import com.example.s528798.simplysiamfinal.Email.GMailSender;
import com.example.s528798.simplysiamfinal.Helper.RecyclerItemTouchHelper;
import com.example.s528798.simplysiamfinal.Interface.RecyclerItemTouchListner;
import com.example.s528798.simplysiamfinal.Model.Order;
import com.example.s528798.simplysiamfinal.ViewHolder.CartAdapter;
import com.example.s528798.simplysiamfinal.ViewHolder.CartViewHolder;
import com.facebook.share.model.AppInviteContent;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.zxing.common.StringUtils;
import com.irozon.sneaker.Sneaker;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Cart extends AppCompatActivity implements RecyclerItemTouchListner,TimePickerDialogFragment.TimePickerDialogHandler {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference reference;
    public TextView totalPrice;
    double mailPrice;
    Button btnPlace;
    ImageView mImageView;
    String emailId;
    List<Order> cart = new ArrayList<>();
    RelativeLayout rootLayout;
    CartAdapter adapter;
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    FirebaseUser user = firebaseAuth.getCurrentUser();
    SwipeRefreshLayout mSwipe;
    MaterialDialog md;
    LinearLayout.LayoutParams lp;
    double total2 = 0.0;
    DatabaseReference myref;
    EditText address;
    String finalTypeofOrder = "";
    String DineOrDelivery = "";
    String noOfCustomers ;
    String typeoforder = "";
    String formatedTime = "23:15";
    EditText time;
    String emailType = "DINE IN";
    String emailTime = "Will be ready in 15 minutes";
    OrderType ot;
    DeliveryAddress da;



    private static final int REQUEST_SELECT_PLACE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        database = FirebaseDatabase.getInstance();
        reference = database.getReference("Requests");
        rootLayout = (RelativeLayout) findViewById(R.id.cartLayout);
        recyclerView = (RecyclerView) findViewById(R.id.listCart);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        ot = new OrderType();

        //Toast.makeText(this, ""+ot.getName(), Toast.LENGTH_SHORT).show();


        totalPrice = (TextView) findViewById(R.id.total);
        btnPlace = (Button) findViewById(R.id.btnPlaceOrder);
        mImageView = (ImageView) findViewById(R.id.cart_image);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback =
                new RecyclerItemTouchHelper(0,ItemTouchHelper.LEFT,this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);



        btnPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cart.size()>0)
                    showAlertDialog();
                else{
                    startActivity(new Intent(Cart.this,EmptyCart.class));
                    finish();
                }
            }
        });

        loadListFood();




    }



    private void showAlertDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Cart.this);
        alertDialog.setTitle("One more step!");
        alertDialog.setMessage("Please provide your email: ");

        LayoutInflater inflater = this.getLayoutInflater();
        View spin = inflater.inflate(R.layout.spinner_cart,null);
        final Spinner spinner = (Spinner) spin.findViewById(R.id.spinner);
        final EditText editText = (EditText) spin.findViewById(R.id.editText);
        time=(EditText) spin.findViewById(R.id.timeDineIn);
        EditText add = (EditText) spin.findViewById(R.id.address);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.planets_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        //Toast.makeText(this, ""+spinner.getSelectedItem().toString().trim(), Toast.LENGTH_SHORT).show();
        String s = ot.getName();


        if(s.equals("DINE")){
            spinner.setSelection(0);
        }
        else if(s.equals("CARRYOUT")){
            spinner.setSelection(1);
        }
        else if(s.equals("DELIVERY")){
            spinner.setSelection(2);
            ot = new OrderType();
            add.setText(ot.getAddress());
        }
        else{
            spinner.setSelection(0);
        }

        address = (EditText) spin.findViewById(R.id.address);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                              @Override
                                              public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                  if(position == 2){
                                                      time.setVisibility(View.INVISIBLE);
                                                      typeoforder = "DELIVERY";
                                                      address.setVisibility(View.VISIBLE);
                                                      address.setFocusable(false);
                                                      emailType = "DELIVERY";
                                                      address.setHint("Please enter your address");
                                                      address.setOnClickListener(new View.OnClickListener() {
                                                          @Override
                                                          public void onClick(View v) {
                                                              try {
                                                                  Intent intent = new PlaceAutocomplete.IntentBuilder
                                                                          (PlaceAutocomplete.MODE_OVERLAY).setBoundsBias(new LatLngBounds(
                                                                          new LatLng(40.345760, -94.892761),
                                                                          new LatLng(40.351190, -94.855923)))
                                                                          .build(Cart.this);

                                                                  startActivityForResult(intent, REQUEST_SELECT_PLACE);
                                                              } catch (GooglePlayServicesRepairableException |
                                                                      GooglePlayServicesNotAvailableException e) {
                                                                  e.printStackTrace();
                                                              }
                                                          }
                                                      });
                                                  }

                                                  else if(position == 0){
                                                      typeoforder = "DINE";
                                                      address.setVisibility(View.VISIBLE);
                                                      address.setInputType(InputType.TYPE_CLASS_NUMBER);
                                                      address.setHint("No of Customers");
                                                      address.setFocusable(true);
                                                      DineOrDelivery = "DINE";
                                                      time.setVisibility(View.VISIBLE);
                                                      time.setOnClickListener(new View.OnClickListener() {
                                                          @Override
                                                          public void onClick(View v) {
                                                             // noOfCustomers = Integer.parseInt(address.getText().toString());
                                                              TimePickerBuilder tpb = new TimePickerBuilder()
                                                                      .setFragmentManager(getSupportFragmentManager())
                                                                      .setStyleResId(R.style.BetterPickersDialogFragment);
                                                              tpb.show();
                                                              emailType = "DINE IN";

                                                          }
                                                      });


                                                  }
                                                  else if(position == 1){
                                                      time.setVisibility(View.INVISIBLE);
                                                      typeoforder = "CARRY";
                                                      address.setVisibility(View.INVISIBLE);
                                                      DineOrDelivery = "CARRYOUT";
                                                      emailType = "CARRY OUT";
                                                  }
                                              }

                                              @Override
                                              public void onNothingSelected(AdapterView<?> parent) {

                                              }
                                          });

        alertDialog.setIcon(R.drawable.emailicon);
        alertDialog.setView(spin);
        mailPrice  = 0.0;
        cart = new Database(Cart.this).getCarts();
        final StringBuilder orderdetails = new StringBuilder();
        for (Order order : cart) {

            mailPrice  += (Double.parseDouble(order.getPrice())) * (Integer.parseInt(order.getQuantity()));
            orderdetails.append("Item:  "+order.getProductName()+"  Quantity: "+order.getQuantity()+"  ItemPrice: "+order.getPrice()+"\n ");

        }

        NumberFormat formatter = new DecimalFormat("#0.00");
        formatter.format(mailPrice);

        final Order order1= new Order();
        emailId = "";
        alertDialog.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (editText.getText().toString().trim().isEmpty()) {
                    //editText.setError("Email Required");
                    NumberFormat formatter = new DecimalFormat("#0.00");
                    formatter.format(mailPrice);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {


                                GMailSender sender = new GMailSender("simplysiamthai@gmail.com",
                                        "simplysiam");
                                sender.sendMail("Simply Siam Order", orderdetails.toString()+"\nOrder Type: "+emailType+"  \nTime: "+emailTime+"\n\nGrand Total: "+ Math.round(mailPrice)+"\n\n" +
                                                "Simply Siam",
                                        "simplysiamthai@gmail.com", "simplysiamthai@gmail.com");
                            } catch (Exception ignored) {

                            }

                        }

                    }).start();

                    md = new MaterialDialog.Builder(Cart.this)
                            .title("Ordering")
                            .cancelable(false)
                            .contentColor(Color.parseColor("#71DA98"))
                            .titleColor(Color.parseColor("#56C5ED"))
                            .content("Please wait.....")
                            .progress(true, 0)
                            .show();

                    myref = database.getReference();
                    Locale locale = new Locale("en", "US");
                    NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
//                    myref.child(user.getUid()).child("Order").setValue(orderdetails.toString());
//                    myref.child(user.getUid()).child("Total").setValue(mailPrice);

                    Intent i = new Intent(Cart.this,ThankYou.class);
                    i.putExtra("Grand Total",mailPrice);
                    i.putExtra("TypeofOrder",finalTypeofOrder);
                    i.putExtra("DineOrDelivery",DineOrDelivery);
                    i.putExtra("Time",formatedTime);
                    if(!TextUtils.isEmpty(address.getText().toString().trim())){
                        if(address.getText().length() == 1){
                        noOfCustomers = String.valueOf(address.getText().toString());}
                        else{
                            noOfCustomers = address.getText().toString();
                        }
                    }
                    i.putExtra("Customers",noOfCustomers);
                    //Toast.makeText(Cart.this, ""+noOfCustomers, Toast.LENGTH_SHORT).show();
                    i.putExtra("TYPE",typeoforder);
                    startActivity(i);
                    finish();

                } else {
                    NumberFormat formatter = new DecimalFormat("#0.00");
                    formatter.format(mailPrice);
                    emailId = editText.getText().toString().trim();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                GMailSender sender = new GMailSender("simplysiamthai@gmail.com",
                                        "simplysiam");
                                sender.sendMail("Simply Siam Order", orderdetails.toString()+"Order Type: "+emailType+"Time: "+emailTime+"\n\nGrand Total: "+ Math.round(mailPrice)+"\n\nYour order will be ready soon!\n" +
                                                "Simply Siam",
                                        "simplysiamthai@gmail.com", emailId);
                                sender.sendMail("Simply Siam Order", orderdetails.toString()+"Order Type: "+emailType+"Time: "+emailTime+"\n\nGrand Total: "+ Math.round(mailPrice)+"\n\n" +
                                                "Simply Siam",
                                        "simplysiamthai@gmail.com", "simplysiamthai@gmail.com");
                            } catch (Exception ignored) {

                            }

                        }



                    }).start();

                    md = new MaterialDialog.Builder(Cart.this)
                            .title("Ordering")
                            .cancelable(false)
                            .contentColor(Color.parseColor("#71DA98"))
                            .titleColor(Color.parseColor("#56C5ED"))
                            .content("Please wait.....")
                            .progress(true, 0)
                            .show();

                    myref = database.getReference();
                    Locale locale = new Locale("en", "US");
                    NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
                    myref.child(user.getUid()).child("Order").setValue(orderdetails.toString());
                    myref.child(user.getUid()).child("Total").setValue(mailPrice);

                    Intent i = new Intent(Cart.this,ThankYou.class);
                    i.putExtra("Grand Total",mailPrice);
                    i.putExtra("TypeofOrder",finalTypeofOrder);
                    i.putExtra("DineOrDelivery",DineOrDelivery);
                    if(!TextUtils.isEmpty(address.getText().toString().trim())){
                        if(address.getText().length() == 1){
                            noOfCustomers = String.valueOf(address.getText().toString());}
                        else{
                            noOfCustomers = address.getText().toString();
                        }
                    }
                    i.putExtra("Customers",noOfCustomers);
                    i.putExtra("TYPE",typeoforder);
                    i.putExtra("Time",formatedTime);
                    startActivity(i);
                    finish();

                }
            }

        }).setCancelable(true);
        alertDialog.create().show();
    }




    private void loadListFood() {

        cart = new Database(this).getCarts();
        adapter = new CartAdapter(cart, this);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        double total = 0.00;
        for (Order order : cart) {
            total += (Double.parseDouble(order.getPrice())) * (Integer.parseInt(order.getQuantity()));
            Locale locale = new Locale("en", "US");
            NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
            totalPrice.setText(" " + fmt.format(total));
            total2 = total;
        }

    }



    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals(Common.DELETE))
            deleteCart(item.getOrder());
        return true;
    }

    private void deleteCart(int position) {
        cart.remove(position);
        new Database(this).clearCart();
        for (Order item : cart) {
            new Database(this).addToCart(item);
        }
        loadListFood();
        if(cart.size() == 0){
            totalPrice.setText(" $0.00");
        }
    }




    @Override
    public void onSwipe(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, int direction, int position) {

    }

    @Override
    public void onDialogTimeSet(int reference, int hourOfDay, int minute) {
        formatedTime =((hourOfDay > 12) ? hourOfDay % 12 : hourOfDay) + ":" + (minute < 10 ? ("0" + minute) : minute) + " " + ((hourOfDay >= 12) ? "PM" : "AM");
        time.setText(formatedTime);
        emailTime = formatedTime;
        if(!TextUtils.isEmpty(address.getText().toString().trim())){
            if(address.getText().length() == 1){
                noOfCustomers = String.valueOf(address.getText().toString());}
            else{
                noOfCustomers = address.getText().toString();
            }
        }
        //Toast.makeText(this, ""+noOfCustomers, Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int adapterPosition) {
        if(viewHolder instanceof CartViewHolder)
        {
            String name = ((CartAdapter)recyclerView.getAdapter()).getItem(viewHolder.getAdapterPosition()).getProductName();
            final Order deleteItem = ((CartAdapter)recyclerView.getAdapter()).getItem(viewHolder.getAdapterPosition());
            final int deleteIndex = viewHolder.getAdapterPosition();
            adapter.removeItem(deleteIndex);
            new Database(getBaseContext()).removeFromCart(deleteItem.getProductId());

            List<Order> orders = new Database(getBaseContext()).getCarts();
            double total = 0.00;
            for (Order item : orders)
                total += (Double.parseDouble(item.getPrice())) * (Integer.parseInt(item.getQuantity()));
            Locale locale = new Locale("en", "US");
            NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
            totalPrice.setText(" " + fmt.format(total));

            Snackbar snackbar = Snackbar.make(rootLayout,name+" is removed from cart",Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapter.restroreItem(deleteItem,deleteIndex);
                    new Database(getBaseContext()).addToCart(deleteItem);
                    List<Order> orders = new Database(getBaseContext()).getCarts();
                    double total = 0.00;
                    for (Order item : orders)
                        total += (Double.parseDouble(item.getPrice())) * (Integer.parseInt(item.getQuantity()));
                    Locale locale = new Locale("en", "US");
                    NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
                    totalPrice.setText(" " + fmt.format(total));
                }
            });
                snackbar.setActionTextColor(Color.YELLOW).show();
        }

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }



    @Override
    public void onPlaceSelected(Place place) {

        //Toast.makeText(this, ""+place, Toast.LENGTH_SHORT).show();
        address.setText(place.getAddress().toString());
        finalTypeofOrder = address.getText().toString();
        ot.setAddress(finalTypeofOrder);




    }

    @Override
    public void onError(Status status) {

        Toast.makeText(this, "Place selection failed: " + status.getStatusMessage(),
                Toast.LENGTH_SHORT).show();
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SELECT_PLACE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                this.onPlaceSelected(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                this.onError(status);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



}
