package com.example.s528798.simplysiamfinal;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.s528798.simplysiamfinal.Common.Common;
import com.example.s528798.simplysiamfinal.Database.Database;
import com.example.s528798.simplysiamfinal.Model.Food;
import com.example.s528798.simplysiamfinal.Model.Order;
import com.example.s528798.simplysiamfinal.Model.Rating;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

import java.lang.reflect.Array;
import java.util.Arrays;

public class FoodDetail extends AppCompatActivity implements RatingDialogListener {

    TextView food_name, food_price, food_description;
    ImageView food_image;
    CollapsingToolbarLayout collapsingToolbarLayout;
    FloatingActionButton btncart,btnRating;
    ElegantNumberButton numberButton;
    Food curentFood;
    String foodId = "";
    RatingBar ratingBar;
    FirebaseDatabase database;
    DatabaseReference food,ratingTbl;
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    FirebaseUser user = firebaseAuth.getCurrentUser();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detail);

        database = FirebaseDatabase.getInstance();
        food = database.getReference("Food");

        numberButton = (ElegantNumberButton) findViewById(R.id.number_button);
        btncart = (FloatingActionButton) findViewById(R.id.btnCart);
        food_description = (TextView) findViewById(R.id.food_description);
        food_name = (TextView) findViewById(R.id.food_name);
        food_price = (TextView) findViewById(R.id.food_price);
        food_image = (ImageView) findViewById(R.id.im_food);

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppbar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppbar);
        btnRating = (FloatingActionButton) findViewById(R.id.btn_rating);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingTbl = database.getReference().child("Rating");


        btnRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRatingDialog();
            }
        });


        btncart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Database(getBaseContext()).addToCart(new Order(

                        foodId,
                        curentFood.getName(),
                        numberButton.getNumber(),
                        curentFood.getPrice(),
                        curentFood.getDiscount(),
                        curentFood.getImage()

                ));

                Toast.makeText(FoodDetail.this, "Added to Cart", Toast.LENGTH_SHORT).show();
            }
        });


        if (getIntent() != null)
            foodId = getIntent().getStringExtra("FoodId");
        if (!foodId.isEmpty()) {
            getDetailFood(foodId);
            getRatingFood(foodId);
        }
    }

    private void getRatingFood(String foodId) {

        Query foodRating = ratingTbl.orderByChild("foodId").equalTo(foodId);
        foodRating.addValueEventListener(new ValueEventListener() {
            int count = 0,sum=0;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot ds:dataSnapshot.getChildren()){
                    Rating item = ds.getValue(Rating.class);
                    sum += Integer.parseInt(item.getRateValue());
                    count++;
                }
                if(count !=0){
                    float average = sum/count;
                    ratingBar.setRating(average);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void showRatingDialog() {

        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                //.setNoteDescriptions(Arrays.asList("Very Bad","Not Good","Quite Ok","Very Good","Excellent"))
                .setDefaultRating(1)
                .setTitle("Rate this application")
                .setDescription("Please give some stars and give your feedback")
                .setTitleTextColor(R.color.colorPrimaryDark)
                .setDescriptionTextColor(R.color.colorPrimaryDark)
                .setHint("Please write your comment here...")
                .setHintTextColor(R.color.white)
                .setCommentTextColor(android.R.color.white)
                .setCommentBackgroundColor(R.color.colorPrimaryDark)

                .setWindowAnimation(R.style.RatingDialogFadeAnim)
                .create(FoodDetail.this).show();



    }

    private void getDetailFood(String foodId) {

        food.child(foodId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                curentFood = dataSnapshot.getValue(Food.class);

                Picasso.with(getBaseContext()).load(curentFood.getImage())
                        .into(food_image);
                collapsingToolbarLayout.setTitle(curentFood.getName());
                food_price.setText(curentFood.getPrice());
                food_name.setText(curentFood.getName());
                food_description.setText(curentFood.getDescription());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Toast.makeText(FoodDetail.this, "Error retrieving Details", Toast.LENGTH_SHORT).show();


            }
        });

    }

    @Override
    public void onNegativeButtonClicked() {


    }

    @Override
    public void onPositiveButtonClicked(int value, String s) {
        Rating rating = new Rating(foodId,String.valueOf(value),s);
        ratingTbl.child(rating.getRateValue()).child(rating.getComment());
    }
}